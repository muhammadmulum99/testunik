import express from "express";
import {
  GetTest
} from "../controllers/Test.js";

const router = express.Router();

router.get("/test", GetTest);
// router.put("/updatevabni", updateVAbni);
// router.post("/inquirybilling", inquiryVAbni);
// router.post("/callbackvabni", CallbackVAbni);
// router.delete('/logout', logOut);

export default router;
