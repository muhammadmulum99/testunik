import db from "../config/Database.js";
import Tb_site_info from "../models/TestModel.js";

export const GetTest = async (req, res) => {
  try {
    const response = await Tb_site_info.findAll();
    // var ciphertextmasbank = cryptoJs.AES.encrypt(
    //   JSON.stringify(response),
    //   "ahmedhikendev2022"
    // ).toString();
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
