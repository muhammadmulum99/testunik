import db from "../config/Database.js";

import Tb_people_count_2023_03 from "../models/Tb_people_count_2023_03.js";

export const GetAll = async (req, res) => {
  try {
    const response = await Tb_people_count_2023_03.findAll();
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};
