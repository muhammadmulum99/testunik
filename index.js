import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import db from "./config/Database.js";
import TestRoute from "./routes/TestRoute.js";
import Teskedua from "./routes/Testkedua.js"


import http from "http";

const hostname = "localhost";
const port = 5000;

// const server = http.createServer((req, res) => {
//   res.statusCode = 200;
//   res.setHeader("Content-Type", "text/plain");
//   res.end("Hello World!\n");
// });

try {
  db.authenticate();
  console.log("Database success connected {-_-}");
} catch (error) {}

dotenv.config();

const app = express();

// const sessionStore = SequelizeStore(session.Store);

// const store = new sessionStore({
//   db: db,
// });

// !enable dibawah kalau belum ada tabel nya
// (async () => {
//   await db.sync();
// })();

// app.use(
//   session({
//     secret: 'dulkijo',
//     resave: false,
//     saveUninitialized: true,
//     store: store,
//     cookie: {
//       secure: "auto",
//     },
//   })
// );

app.use(
  cors({
    credentials: true,
    origin: ["http://localhost:3000", "http://localhost:3001"],
  })
);
// app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(express.json());
app.use(TestRoute);
app.use(Teskedua);
// ! ini juga
// store.sync();

// server.listen(process.env.APP_PORT, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });

app.listen(port, hostname, () => {
  console.log(
    `Server up and runningn at http://${hostname}:${port}/   {-_-}`
  );
});

